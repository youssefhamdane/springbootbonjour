FROM openjdk:19-jdk-alpine3.15
ARG JAR_FILE=out/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-cp","/app.jar","com.mus.iam.IamApplication"]