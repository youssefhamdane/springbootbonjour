package com.mus.iam.entities;

import javax.persistence.*;

@Entity
@Table(name = "Exemple")
public class Exemple {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "description")
    private String description;

    protected Exemple() {
    }

    protected Exemple(String nom, String description) {
        this.nom = nom;
        this.description = description;
    }

    @Override
    public String toString() {
        return id + ". " + nom + " - " + description + " USD";
    }
}